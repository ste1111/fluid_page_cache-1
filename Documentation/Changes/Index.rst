.. include:: ../Includes.txt


.. _changes:


Changes
=======

1.1.0
-----

- [FEATURE] Add TYPO3 v10 compatibility
- [DEVOPS] Add DDEV environment
- [DEVOPS] Remove Vagrantfile


1.0.0
-----

First release of **fluid_page_cache** extension with TYPO3 8.7 LTS and 9.5 LTS support.
